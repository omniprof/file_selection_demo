package com.kenfogel.file_selection_demo;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Example of using a file chooser
 *
 * Here is a more complete example
 * https://examples.javacodegeeks.com/desktop-java/javafx/javafx-filechooser-example/
 *
 * @author Ken Fogel
 */
public class MainAppFX extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    @Override
    public void start(Stage primaryStage) {
        try {
            // Instantiate the FXMLLoader
            FXMLLoader loader = new FXMLLoader();

            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(MainAppFX.class.getResource("/fxml/FileFXML.fxml"));

            // Parent is the base class for all nodes that have children in the
            // scene graph such as AnchorPane and most other containers
            Parent parent = (BorderPane) loader.load();

            // Load the parent into a Scene
            Scene scene = new Scene(parent);

            // Put the Scene on Stage
            primaryStage.setScene(scene);
            primaryStage.show();
            
            primaryStage.setOnCloseRequest((WindowEvent we) -> {
                Platform.exit();
            });
        } catch (IOException ex) { // getting resources or files
            // could fail
            LOG.error(null, ex);
            System.exit(1);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
