package com.kenfogel.file_selection_demo.controller;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class for demonstrating FileChooser
 *
 * Here is a more complete example
 * https://examples.javacodegeeks.com/desktop-java/javafx/javafx-filechooser-example/
 *
 * @author Ken Fogel
 */
public class FileFXMLController {

    private FileChooser fileChooser;
    private Stage stage;

    @FXML
    private BorderPane borderPane;

    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        fileChooser = new FileChooser();

    }

    /**
     * Event handler for the Load menu item
     *
     * @param event
     */
    @FXML
    void onLoad(ActionEvent event) {
        // We need the satge to open a file chooser
        // The stage can be extracted from any component that is currently
        // visible. The root pane is a good choice but even a button can be
        // used to get the stage.
        stage = (Stage) borderPane.getScene().getWindow();

        // Open the FileChooser dailog
        File file = fileChooser.showOpenDialog(stage);
        // Dialog returns a File object if a file is selected and null if the
        // dialog is cancelled without a selection
        if (file != null) {
            // Just displaying the absolute path
            System.out.println("Absolute Path: " + file.getAbsolutePath());
        }
    }

    @FXML
    void onSave(ActionEvent event) {
        // ToDo
    }
}
